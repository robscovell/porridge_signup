#!/usr/bin/perl -w

use XML::WBXML;
use LWP::UserAgent;
use Data::Dumper;

my $username = 'dccoms';
my $password = 'dc123';

my $destination = '447733383841';
my $user = '02071480000';
my $pass = '123gjm';

my $udh_prefix = '0B05040B8423F0000384';


open IN, "<test.xml";

my $xml;

while (<IN>) {
    s/\%user\%/$user/;
    s/\%pass\%/$pass/;
    $xml .= $_;
}

#warn $xml;

my $wbxml = XML::WBXML::xml_to_wbxml($xml);
my $udp = '0106031F01B6' . ascii_to_hex($wbxml);

#my $wbxml = `xml2wbxml -v 1.1 -o - test.xml`;

#my $udp = '0106041f2db600' . ascii_to_hex($wbxml);


my $length = length ($udh_prefix . $udp);
my $n = int($length / 160) + 1;

if ($n < 16) {
    $udh_prefix .= '0' . uc(sprintf("%x", $n));
}
else {
    $udh_prefix .= uc(sprintf("%x", $n));
}

my $data = {};

warn Dumper $udp;

for my $i (1 ... $n) {
    $data->{$i}->{'udp'} = substr($udp, ($i-1) * 160, 160);
    if ($i < 16) {
        $data->{$i}->{'udh'} = $udh_prefix . '0' . uc(sprintf("%x", $i));
    }
    else {
        $data->{$i}->{'udh'} = $udh_prefix . uc(sprintf("%x", $i));
    }
}

warn Dumper $data;

my $ua = LWP::UserAgent->new();

foreach my $i (1 ... $n) {

    my $params = {
        'udh' => $data->{$i}->{'udh'}, 
        'data' => $data->{$i}->{'udp'}, 
        'username' => $username, 
        'password' => $password,
        'destination' => $destination
    };

    my $response = $ua->post( "http://gw1.aql.com/sms/gw-udh.php", $params );

    warn $response->content;

}

sub ascii_to_hex {
    ## Convert each ASCII character to a two-digit hex number.
    (my $str = shift) =~ s/(.|\n)/sprintf("%02lx", ord $1)/eg;
    return $str;
}

superline
